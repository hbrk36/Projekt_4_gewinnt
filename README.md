VIER GEWINNT

Bei Vier gewinnt versucht jeder Spieler vier Steine seiner Farbe (Rot oder Gelb) in einer Reihe zu platzieren - vertikal, horizontal oder diagonal.

In unserem Programm muss es also möglich sein, abwechselnd einen Kreis setzen zu können.

Die größte technische Herausforderung wird sein, das Kreise aufeinander stehen bleiben.

Das Programm muss ebenfalls erkennen, ob ein Spieler die gewonnen hat.



Die Herausforderung wird sein, dass das Programm 2 Spielern ermöglichen muss,
abwechselnd Kreise zu setzen, die auf dem Feld nach unten wandern, 
und stoppen, wenn das Ende erreichen oder auf ein anderer Kreis treffen.

Gleichzeitig muss das Programm erkennen ob ein Spieler gewonnen hat oder nicht.
